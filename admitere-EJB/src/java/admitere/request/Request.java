

package admitere.request;

import java.util.List;
import javax.ejb.Remote;


@Remote
public interface Request {

    void createUser( 
            String firstName,
            String lastName, 
            String title, 
            String password);

}


package admitere.request;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import admitere.entity.*;


/**
 * This is the bean class for the RequestBean enterprise bean.
 */
@Stateful
public class RequestBean implements Request {
    private static final Logger logger = Logger.getLogger(
                "admitere.request.RequestBean");
    @PersistenceContext
    private EntityManager em;

    public void createUser(
            String firstName,
            String lastName, 
            String title, 
            String password) 
    {
        logger.info("createPlayer");

        try {
            User player = new User(firstName, lastName, title, password);
            em.persist(player);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    
}
